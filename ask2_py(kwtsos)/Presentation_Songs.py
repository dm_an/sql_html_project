from pymysql import connect, err, sys, cursors
from bottle import route, run, static_file, request, get, post
import app

def a_Table(tuples):
    printResult = """<style type='text/css'> h1 {color:red;} h2 {color:blue;} p {color:green;} </style>
    <table border = '1' frame = 'above'>"""

    header='<tr><th>'+'</th><th>'.join([str(x) for x in tuples[0]])+'</th></tr>'
    data='<tr>'+'</tr><tr>'.join(['<td>'+'</td><td>'.join([str(y) for y in row])+'</td>' for row in tuples[1:]])+'</tr>'
        
    printResult += header+data+"</table>"
    return printResult
    

@route('/presentationSongs')
def presentationSongsWEB():
    title = request.query.nat_id
    prod_year = request.query.prod_year
    company = request.query.company
    
    table = app.presentationSongs(title, prod_year, company)
    return "<html><body>" + a_Table(table) + "</body></html>"
    


@route('/:path')
def callback(path):
    return static_file(path, 'web')

@route('/')
def callback():
    return static_file("Presentation_Songs.html", 'web')
    

run(host='localhost', port=9090, reloader=True, debug=True)
#http://localhost:9090
