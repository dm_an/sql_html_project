xrhsh arxeiwn py kai html

*website.py
    imports
    @route gia kathe selida
    p.x.
        @route('/main')
        def show_main():
            return template("index.html")
    @post gia kathe action formas
    p.x.
        @post('/action1')
        def action1():
            return ...
    run(...)

Pws vlepoume tin selida (paradeigma):
=>terminal: python website.py
=>browser: localhost:9090

*application.py
    imports
    voithiktikes synarthseis gia to website.py

*html
vasiki domi gia formes:

<form action="/action" method="post">

    Name: <input type="text" name="onoma" /><br />
    Surname: <input type="text" name="epitheto" />
    ...
    <input type="submit" name="submit" value="Submit" />

</form>

-> to koumpi allagis einai to koumpi submit formas me hidden pedia
-> <input type="hidden" name="kati" value="timh" />
