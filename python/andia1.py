from pymysql import connect, err, sys, cursors
from bottle import Bottle, route, run, template

#Doing our connection
conn = connect(host='localhost',
                    port=3308,
                    user='root',
                    password='andiakonstantina123',
                    db='songs',
                    charset='utf8')

#Setting cursors and defining type of returns we need from Database
#here it's gonna be returning as dictionary data
cursor = conn.cursor(cursors.DictCursor);

cursor.execute("SELECT * FROM cd_production")
data=cursor.fetchall()
print(data)


