from bottle import route, run
 
@route('/<path1>/<path2>')
def default(path1, path2):
    return '<strong>Path 1: %(path1)s<br>Path 2: %(path2)s</strong>' % {"path1": path1, "path2": path2}
 
run(host='localhost', port=8080)
#</path2></path1>
