from bottle import Bottle, route, run, template
 
@route('/<unknown>')
def default(unknown):
    return '<strong>Unknown: %s</strong>' % unknown
 
run(host='localhost', port=8080)
#'</unknown>
